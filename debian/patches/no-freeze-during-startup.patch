Author: Brian Raiter <breadbox@muppetlabs.com>
        Andreas Tille <tille@debian.org>
Date: Wed, 18 Sep 2024 07:30:34 +0200
Bug-Debian: https://bugs.debian.org/1074389
Description: Fix freeze during startup
ROOT CAUSE:

The program is attempting to change the window title without checking
if the window exists yet. This in itself would be no more than a
cosmetic problem, except that the code is temporarily borrowing the X
server connection from SDL (taking advantage of an advanced,
semi-documented feature of the library). SDL does not have code to
properly handle the error event that results from the unexpected
invalid request, and fails to recover properly.

FIX:

The fix is simply to add a test to avoid making the X request if the
window ID is still zero. The existing code already has a fallback for
changing the title using the standard SDL1.2 API if the X11 API is
unusable, so the window title will still get set with this change.


--- a/src/gengine/SysVideo.cpp
+++ b/src/gengine/SysVideo.cpp
@@ -57,20 +57,22 @@ sysSetCaption(SDL_SysWMinfo *info, const
     if (info->subsystem == SDL_SYSWM_X11) {
         info->info.x11.lock_func();
 
-        XTextProperty titleprop;
-        char *text_list = const_cast<char*>(title.c_str());
-        int error = Xutf8TextListToTextProperty(info->info.x11.display,
-                &text_list, 1, XUTF8StringStyle, &titleprop);
-        if (!error) {
-            XSetWMName(info->info.x11.display, info->info.x11.wmwindow,
-                    &titleprop);
-            XFree(titleprop.value);
-            result = true;
-        }
-        else {
-            LOG_DEBUG(ExInfo("not supported conversion")
-                    .addInfo("error", error)
-                    .addInfo("title", title));
+        if (info->info.x11.wmwindow) {
+            XTextProperty titleprop;
+            char *text_list = const_cast<char*>(title.c_str());
+            int error = Xutf8TextListToTextProperty(info->info.x11.display,
+                    &text_list, 1, XUTF8StringStyle, &titleprop);
+            if (!error) {
+                XSetWMName(info->info.x11.display, info->info.x11.wmwindow,
+                        &titleprop);
+                XFree(titleprop.value);
+                result = true;
+            }
+            else {
+                LOG_DEBUG(ExInfo("not supported conversion")
+                        .addInfo("error", error)
+                        .addInfo("title", title));
+            }
         }
 
         info->info.x11.unlock_func();
